<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Metcast\IncomeSource\OpenWeather;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\DataFixtures\ORM\LoadUserData;
use AppBundle\Metcast\DTO\Weather;

class OpenWeatherTest extends WebTestCase
{
    public function testRating()
    {
        $weather = new OpenWeather();
        $weather->data = '{"city":{"id":2950159,"name":"Berlin","coord":{"lon":13.41053,"lat":52.524368},"country":"DE","population":0},"cod":"200","message":0.0241,"cnt":7,"list":[{"dt":1461495600,"temp":{"day":10.75,"min":4.8,"max":10.75,"night":4.8,"eve":9.02,"morn":9.97},"pressure":1012.04,"humidity":77,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":3.32,"deg":281,"clouds":44,"rain":2.04},{"dt":1461582000,"temp":{"day":10.22,"min":4.43,"max":10.22,"night":4.43,"eve":6.17,"morn":5.85},"pressure":1010.14,"humidity":81,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":5.36,"deg":247,"clouds":24,"rain":1.33},{"dt":1461668400,"temp":{"day":5.26,"min":1.44,"max":6.94,"night":1.44,"eve":4.38,"morn":4.75},"pressure":1001.52,"humidity":100,"weather":[{"id":600,"main":"Snow","description":"light snow","icon":"13d"}],"speed":4.74,"deg":200,"clouds":80,"rain":4.73,"snow":0.86},{"dt":1461754800,"temp":{"day":4.88,"min":2.96,"max":5.6,"night":2.96,"eve":5.6,"morn":3.45},"pressure":1005.83,"humidity":100,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":3.56,"deg":186,"clouds":92,"rain":1.05},{"dt":1461841200,"temp":{"day":7.91,"min":3,"max":8.76,"night":3,"eve":5.86,"morn":3.43},"pressure":1017.87,"humidity":91,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":4.46,"deg":259,"clouds":32,"rain":7.15},{"dt":1461927600,"temp":{"day":8.49,"min":3.43,"max":9.13,"night":3.43,"eve":9.13,"morn":7.59},"pressure":1021.33,"humidity":0,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":7.75,"deg":239,"clouds":65,"rain":3.74},{"dt":1462014000,"temp":{"day":14.47,"min":6.25,"max":14.47,"night":8.63,"eve":12,"morn":6.25},"pressure":1023.73,"humidity":0,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":6.11,"deg":169,"clouds":27,"rain":5.59}]}';
        $items = $weather->getWeatherItems("Berlin");
        $this->assertCount(7, $items);
        /** @var Weather $item */
        $item = $items[0];
        $this->assertEquals("http://openweathermap.org/img/w/10d.png", $item->icon);
        $this->assertEquals(10.8, $item->temp);
        $this->assertEquals("Berlin, DE", $item->city);
        $date = $item->date;
        $this->assertEquals("2016-04-24", $date->format("Y-m-d"));
    }
}