$( document ).ready(function() {
    if ($("#city").val().length == 0) {
        $.get("http://ipinfo.io", function (response) {
            $("#city").val(response.city);
        }, "jsonp");
    }

    $('#form').ajaxForm(function(data) {
        $("#weather").html(data);
    });
});