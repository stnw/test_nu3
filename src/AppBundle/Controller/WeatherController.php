<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WeatherController extends Controller
{
    /**
     * @Route("weather", name="weather_show")
     */
    public function showAction(Request $request)
    {
        return $this->render('weather/show.html.twig', []);
    }

    /**
     * @Route("weather/ajax/show", name="weather_show_ajax")
     */
    public function generateAction(Request $request)
    {
        $items = [];
        if ($request->get('city')) {
            $items = $this->get('app.weather')->getWeatherItems($request->get('city'));
        }
        return $this->render('weather/show_ajax.html.twig', [
                "items" => $items
            ]
        );
    }
}