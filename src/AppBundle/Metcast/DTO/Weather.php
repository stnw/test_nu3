<?php

namespace AppBundle\Metcast\DTO;


class Weather
{
    public $city;

    public $temp;

    public $date;

    public $icon;
}