<?php

namespace AppBundle\Metcast\IncomeSource;

use AppBundle\Metcast\DTO\Weather;

class OpenWeather implements IncomeSourceInterface
{
    private $APIKey = "06fe098afd19c2793b85a39fdfcd776a";

    public $data;

    /**
     * @param $cityName
     * @return @return Weather[]
     */
    public function getWeatherItems($cityName)
    {
        $data = json_decode($this->getData($cityName));
        $items = [];
        foreach ($data->list as $item) {
            $weather = new Weather();
            $weather->temp = round($item->temp->day, 1);
            $weather->icon = sprintf("http://openweathermap.org/img/w/%s.png", $item->weather[0]->icon);
            $weather->city = $data->city->name . ", " . $data->city->country;
            $date = new \DateTime();
            $date->setTimestamp($item->dt);
            $weather->date = $date;
            $items[] = $weather;
        }

       return $items;
    }

    private function getData($cityName)
    {
        //mock for tests
        if ($this->data) {
            return $this->data;
        }

        $url = sprintf("http://api.openweathermap.org/data/2.5/forecast/daily?q=%s&APPID=%s&units=metric&cnt=3", $cityName, $this->APIKey);
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER,true);
        return curl_exec($session);
    }
}