<?php

namespace AppBundle\Metcast\IncomeSource;

interface IncomeSourceInterface
{
    /**
     * @param $cityName
     * @return @return Weather[]
     */
    public function getWeatherItems($cityName);
}